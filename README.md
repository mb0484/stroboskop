# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://mb0484@bitbucket.org/mb0484/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/mb0484/stroboskop/commits/4539f225a784462b1de36ffa7c994574b2d4690e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mb0484/stroboskop/commits/42f5d258ebec84530cc1925861ff42d635bb22cf

Naloga 6.3.2:
https://bitbucket.org/mb0484/stroboskop/commits/ac058798163a989cbfc5f9c39576606ab1359f94

Naloga 6.3.3:
https://bitbucket.org/mb0484/stroboskop/commits/a8ba340a9ba59c86bcc23d733c085d246ba8e46a

Naloga 6.3.4:
https://bitbucket.org/mb0484/stroboskop/commits/3843f5d12688581bfff65fac4788d90db8fb4016

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push

```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mb0484/stroboskop/commits/296d1a34521467a1ed109b8c8454059b2c3bbf31

Naloga 6.4.2:
https://bitbucket.org/mb0484/stroboskop/commits/de547fcb231657cc56abc56cc5b646b7d949e522

Naloga 6.4.3:
https://bitbucket.org/mb0484/stroboskop/commits/aa5292558ce69241dc673351cbf9518990138a8c

Naloga 6.4.4:
https://bitbucket.org/mb0484/stroboskop/commits/837abffff1e66e12b72d3394e802fdf02097c9fd